Dart è già fornito con funzionalità HttpServer di base. Il nuovo Web Socket si basa su HttpServer, quindi si inserisce direttamente in un'app Dart Server esistente.

Il frammento di codice seguente è un semplice esempio di un server echo Web Socket in esecuzione in Dart.
Una volta eseguita questa operazione, è possibile utilizzare una semplice pagina di test Web Socket per connettersi e ricevere messaggi di eco. Ad esempio, se si esegue il codice sopra riportato, è possibile connettersi a ws: // localhost: 8000 / ws