import 'package:draganddrop/dropcity/draggable.dart';
import 'package:draganddrop/dropcity/dragbox.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

final countries = [
  new Trasc(0),
  new Trasc(1),
  new Trasc(2)
];

void main() {
  runApp(new DropCityApp(countries));
}

class DropCityApp extends StatelessWidget {
  List<Trasc> items;

  DropCityApp(this.items);

  @override
  Widget build(BuildContext context) {
    SystemChrome.setPreferredOrientations([
      DeviceOrientation.portraitUp,
    ]);
    return new MaterialApp(
        debugShowCheckedModeBanner: false,
        theme: _getTheme(context),
        home: new Scaffold(
          appBar: AppBar(
          title: const Text('Drag&Drop'),
          ),
          body: new GameView(items),
        ));
  }

  ThemeData _getTheme(BuildContext context) => Theme.of(context).copyWith(
      textTheme: new TextTheme(
          body1: new TextStyle(fontSize: 16.0, color: Colors.grey.shade700)));
}
