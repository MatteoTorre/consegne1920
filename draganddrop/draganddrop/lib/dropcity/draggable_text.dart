import 'package:draganddrop/dropcity/draggable.dart';
import 'package:draganddrop/dropcity/draggable_view.dart';
import 'package:flutter/material.dart';

class DraggableCity extends StatefulWidget {
  final Trasc item;
  final Size size;

  bool enabled = true;
  DraggableCity(this.item, {this.size});

  @override
  _DraggableCityState createState() => new _DraggableCityState();
}

class _DraggableCityState extends State<DraggableCity> {

  double _width(int id){
    double ris;
    if(id==0){return ris=60;}
    else if (id==1){return ris=80;}
    else {return ris=120;}
  }

  double _height(int id){
    double ris;
    if(id==0){return ris=60;}
    else if (id==1){return ris=80;}
    else {return ris=120;}
  }

  Size _size(int id){
    Size ris;
    if(id==0){return ris=new Size(60,60);}
    else if (id==1){return ris=new Size(80,80);}
    else {return ris=new Size(120,120);}
  }
  
  double _opacity(bool a) {
    double ris;
    if (a) {
      ris = 0.0;
      return ris;
    } else {
      ris = 1.0;
      return ris;
    }
  }

  @override
  Widget build(BuildContext context) {
    return new Padding(
        padding: new EdgeInsets.all(4.0),
        child: new Draggable<Trasc>(
            onDraggableCanceled: (velocity, offset) {
              setState(() {
                widget.item.selected = false;
                widget.item.status = Status.none;
              });
            },
            childWhenDragging: new DragAvatarBorder(
              new Opacity(
                opacity: 0.0,
                child: new Text(widget.item.id.toString())),
                color: Colors.grey[200], size: widget.size),
            child: new Container(
                width: _width(widget.item.id),
                height: _height(widget.item.id),
                color: widget.item.selected ? Colors.grey : Colors.cyan,
                ),
            data: widget.item,
            feedback: new DragAvatarBorder(
              new Opacity(
                opacity: 0.0,
                child: new Text(widget.item.id.toString(),
                  style: new TextStyle(
                      fontSize: 16.0,
                      color: Colors.white,
                      decoration: TextDecoration.none))),
              size: _size(widget.item.id),
              color: Colors.cyan,

            )));
  }
}
