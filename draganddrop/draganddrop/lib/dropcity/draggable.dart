import 'package:flutter/material.dart';

enum Status { none, correct, wrong }

class Trasc {
  final int id;
  bool _selected = false;

  bool get selected => _selected;

  void set selected(bool value) {
    _selected = value;
    if( _selected == false )
      status = Status.none;
  }
  Status status;

  Trasc(this.id, {this.status: Status.none});

  @override
  String toString() {
    return 'Item{id: $id, '
      ' selected: $selected, status: $status}';
  }
}

Color getDropBorderColor(Status status) {
  Color c;
  switch (status) {
    case Status.none:
      c = Colors.grey[300];
      break;
    case Status.correct:
      c = Colors.lime[300];
      break;
    case Status.wrong:
      c = Colors.orange[300];
      break;
  }
  return c;
}