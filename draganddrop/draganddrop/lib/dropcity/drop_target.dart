import 'package:draganddrop/dropcity/draggable.dart';
import 'package:draganddrop/dropcity/draggable_view.dart';
import 'package:flutter/material.dart';

typedef void DropItemSelector(Trasc item, DropTarget target);

class SelectionNotification extends Notification {
  final int dropIndex;
  final Trasc item;
  final bool cancel;

  SelectionNotification(this.dropIndex, this.item, {this.cancel: false});
}

class DropTarget extends StatefulWidget {
  final Trasc item;

  final Size size;
  final Size itemSize;

  Trasc _selection;

  Trasc get selection => _selection;

  get id => item.id;

  set selection(Trasc value) {
    clearSelection();
    _selection = value;
  }

  DropTarget(this.item, {this.size, Trasc selectedItem, this.itemSize}) {
    _selection = selectedItem;
  }
  @override
  _DropTargetState createState() => new _DropTargetState();

  void clearSelection() {
    if (_selection != null) _selection.selected = false;
  }
}

class _DropTargetState extends State<DropTarget> {
  static const double kFingerSize = 50.0;

  @override
  Widget build(BuildContext context) {
    return new Padding(
        
        padding: new EdgeInsets.all(4.0),
        child:
            widget.selection != null ? addDraggable(getTarget()) : getTarget());
  }



  Widget addDraggable(DragTarget target) => new Draggable<Trasc>(
      data: widget.selection,
      dragAnchor: DragAnchor.pointer,
      onDraggableCanceled: onDragCancelled,
      feedback: getCenteredAvatar(),
      child: target);


  Widget _paintDT(int id, Color colore) {
    if (id == 1) {
      return CustomPaint(
          painter: CurvePainter(
              a: 0.310, b: 0.160, colore: colore), //quadrato centrale
              );

    } else if (id == 2) {
      return CustomPaint(
          painter: CurvePainter(a: 0.150, b: 0, colore: colore)); //quadrato dx
    } else {
      return CustomPaint(
          painter:
              CurvePainter(a: 0.470, b: 0.320, colore: colore)); //quadrato sx
    }
  }

  DragTarget getTarget() => new DragTarget<Trasc>(
      onWillAccept: (item) => widget.selection != item,
      onAccept: (value) {
        new SelectionNotification(widget.item.id, value).dispatch(context);
      },
      builder: (BuildContext context, List<Trasc> accepted,
          List<dynamic> rejected) {
        return new SizedBox(
            child: new Container(
                width: widget.size.width,
                height: widget.size.height,
                decoration: new BoxDecoration(
                    color: accepted.isEmpty
                        ? (widget.selection != null
                            ? getDropBorderColor(widget.selection.status)
                            : Colors.grey[300])
                        : Colors.cyan[100],
                    border: new Border.all(
                        width: 2.0,
                        color:
                            accepted.isEmpty ? Colors.grey : Colors.cyan[300])),
                child: widget.selection != null
                    ? new Stack(
                        children: <Widget>[
                          new SizedBox(
                            width: widget.size.width,
                            height: widget.size.height,
                            child: _paintDT(widget.item.id,Colors.red),
                          ),
                          new Padding(
                            padding: new EdgeInsets.only(top:_top(widget.selection.id), left:_left(widget.selection.id)), 
                            child: new Container(
                              width: _width(widget.selection.id),
                              height: _height(widget.selection.id),
                              decoration: myBoxDecoration(),
                            ),
                          ),

                        ],
                      )
                    : _paintDT(widget.item.id,Colors.red)));
      });

  double _top(int id){
    double ris;
    if(id==0){return ris=150;}
    else if (id==1){return ris=130;}
    else {return ris=80;}
  }

  double _left(int id){
    double ris;
    if(id==0){return ris=22;}
    else if (id==1){return ris=12;}
    else {return ris=0;}
  }

  double _width(int id){
    double ris;
    if(id==0){return ris=60;}
    else if (id==1){return ris=80;}
    else {return ris=120;}
  }

  double _height(int id){
    double ris;
    if(id==0){return ris=60;}
    else if (id==1){return ris=80;}
    else {return ris=120;}
  }

  BoxDecoration myBoxDecoration(){
     return BoxDecoration(color: Colors.blue);
  }

  void onDragCancelled(Velocity velocity, Offset offset) {
    setState(() {
      widget.selection = null;
      new SelectionNotification(widget.item.id, widget.selection, cancel: true)
          .dispatch(context);
    });
  }

  Widget getCenteredAvatar() => new Transform(
      transform: new Matrix4.identity()
        ..translate(-100.0 / 2.0, -(100.0 / 2.0)),
      child: new DragAvatarBorder(
        new Text(widget.selection?.id.toString(),
            style: new TextStyle(
                fontSize: 16.0,
                color: Colors.white,
                decoration: TextDecoration.none)),
        size: widget.itemSize,
        color: Colors.cyan,
      ));
}

class CurvePainter extends CustomPainter {
  double a;
  double b;
  Color colore;
  CurvePainter(
      {Key key, @required this.a, @required this.b, @required this.colore});
  @override
  void paint(Canvas canvas, Size size) {
    var paint = Paint();
    paint.color = colore;
    paint.strokeWidth = 2.0;

    var path = Path();
    double _trapezeHeightsx = size.height * a;
    double _trapezeHeightdx = size.height * b;
    path.moveTo(0, _trapezeHeightsx);
    path.lineTo(0, size.height);
    path.lineTo(size.width, size.height);
    path.lineTo(size.width, _trapezeHeightdx);
    canvas.drawPath(path, paint);
  }

  @override
  bool shouldRepaint(CustomPainter oldDelegate) {
    return true;
  }
}
